package com.app.Repository;

import org.springframework.data.repository.CrudRepository;
import com.app.Domain.Film;

public interface FilmRepository extends CrudRepository<Film, Integer>
{
}

