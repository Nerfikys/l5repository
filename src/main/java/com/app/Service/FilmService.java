package com.app.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.Domain.Film;
import com.app.Repository.FilmRepository;
//defining the business logic
@Service
public class FilmService
{
    @Autowired
    FilmRepository filmRepository;
    //getting all student records
    public List<Film> getAllStudent()
    {
        List<Film> films = new ArrayList<Film>();
        filmRepository.findAll().forEach(film -> films.add(film));
        return films;
    }
    //getting a specific record
    public Film getStudentById(int id)
    {
        return filmRepository.findById(id).get();
    }
    public void saveOrUpdate(Film film)
    {
        filmRepository.save(film);
    }
    //deleting a specific record
    public void delete(int id)
    {
        filmRepository.deleteById(id);
    }
}