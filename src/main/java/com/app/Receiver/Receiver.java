package com.app.Receiver;

import com.app.Domain.Film;
import com.app.Sender.Sender;
import com.app.Service.FilmService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.Integer.parseInt;

@RabbitListener(queues = "c2r")
public class Receiver {

    //autowired the StudentService class
    @Autowired
    FilmService filmService;

    @Autowired
    Film film;

    @Autowired
    Sender sender;

    @RabbitHandler
    public void receive(String message) {
        String[] buffer = message.split("-");

        film.setId(parseInt(buffer[0]));
        film.setTime(parseInt(buffer[1]));
        film.setName(buffer[2]);

        filmService.saveOrUpdate(film);
        System.out.println(" [x] Received '" +
                film.getId()+"-"+
                film.getTime()+"-"+
                film.getName()+"'");

        sender.send("created field with {" +
                "id: " + film.getId()+
                ", age: " + film.getTime()+
                ", name: " + film.getName());
    }
}
